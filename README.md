# README #

* Za otvaranje aplikacije po mogućstvu koristiti Firefox jer Chrome blokira aplikaciju zbog SSL sertifikata.
* Backend se pokreće naredbom 'dotnet run' iz konzole sa lokacije '..\projekat3\frontend\frontend'. Ulkoliko nisu instalirani paketi potrebno je izvršiti i naredbu 'dotnet restore'.
* Frontend se pokreće naredbom 'npm start' iz konzole sa lokacije '..\projekat3\backend'. Ulkoliko nisu instalirani paketi potrebno je izvršiti i naredbu 'npm install'.

### Users ###

* Ime (username:password)
* Miloš Petković (admin:admin)
* Hedonist (hed:hedForever100)
* Krilce i pivce (krilce:pivce)
* Hush hush (hush:hushNis)
* Planinarski dom (planinarski:planinarski18000)
* Intergalactic Diner (intergalactic:diner11)
* Detroit bar(DetroitBar:motorCity)
* Carpe Diem (CarpeDiem:carpe)
* Casual(casual:casual)
* Dragon Caffe(Dragon:Dragon)
* SkyLounge(SkyLounge:SkyLounge)
* Restoran Dolly Bell(Dolly:Belly)
* Savka (savka:vilaSavka)
* Maratonac (maratonac:trciPocasniKrug)
* Pizzeria Master (master:masterWrap)
* Turrisi (turrisi:barTurrisi)
* Kamiondžije (kamiondzije:kamiondzije)
* Giardino (giardino:giardino5)
* Vero (vero:veroRuma)
* Zig zag (zigzag:zigzagBg)
This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact