import React from 'react'
import EditObjekat from "../pages/EditObjekat";
import { useParams } from "react-router-dom";

export default function EEditObjekat() {
    const { objekatId } = useParams();
    return (
        <div>
            <EditObjekat id={objekatId}/>
        </div>
    )
}
