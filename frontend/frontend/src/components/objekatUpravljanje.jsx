import React from 'react'
import ObjekatUpravljanje from "../pages/ObjekatUpravljanje";
import { useParams } from "react-router-dom";

export default function ObjekatUpr() {
    const { objekatId } = useParams();
    return (
        <div>
            <ObjekatUpravljanje id={objekatId} />
        </div>
    )
}
