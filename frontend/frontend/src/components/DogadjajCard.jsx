import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import dogadjajService from '../services/dogadjaj.service';

const useStyles = makeStyles({
  root: {
    maxWidth: 450,
    minWidth: 200,
  },
  media: {
    height: 140,
  },
});

export default function DogadjajCard(props) {

  const classes = useStyles();

  const isOwner = props.user && props.user.role === "Objekat" && props.user.nameid === props.objekatId;
  const isAdmin = props.user && props.user.role === "Admin";
  const isAllowed = isOwner || isAdmin;

  const handleDelete=(ev)=>{
    dogadjajService.obrisiDogadjaj(props.id)
    .then(res=>{
        console.log(res);
        console.log(res.data);
        props.uzmiDogadjaje();
    }).catch(error => {
      console.log(error);
    });
  }

  return (
    <Card className={classes.root} raised>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
            {props.naziv}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
            {props.vremeOdrzavanja.toUTCString()}
        </Typography>
        <Typography variant="body2" component="p">
            {props.opis}
        </Typography>
      </CardContent>
      <CardActions>
        {isAllowed && (
          <Button size="small" onClick={handleDelete}><DeleteIcon /></Button>
        )}
      </CardActions>
    </Card>
  );
}