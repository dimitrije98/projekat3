import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import objekatService from '../services/objekat.service';
import authService from '../services/auth.service';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    maxHeight: 290,
  },
  media: {
    height: 140,
  },
});

export default function ObjekatCard(props) {

  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  const isOwner = props.user && props.user.role === "Objekat" && props.user.nameid === props.id;
  const isAdmin = props.user && props.user.role === "Admin";
  const isAllowed = isOwner || isAdmin;

  const handleDelete=(ev)=>{
    objekatService.obrisiObjekat(props.id)
      .then(res => {
          console.log(res);
          console.log(res.data);
          props.uzmiObjekte();
          if(isOwner) {
            authService.logout();
            window.location.reload();
          }
            
      }).catch(error => {
        console.log(error);
    });
  }

  return (
    <Card className={classes.root} raised style={{ margin: 8 }}>
      <CardContent>
      <Typography gutterBottom variant="h5" component="h2">
            {props.ime}
      </Typography>
      <Typography variant="body2" color="textSecondary" component="p">
        {props.oznake.map(oznaka => <div key={oznaka}>{bull} {oznaka} </div>)}
        <div>Adresa: {props.adresa.ulica} {props.adresa.broj}, {props.adresa.grad} </div>
      </Typography>
      <Typography variant="body2" component="p">
      Trenutno stanje: <br />
        {props.stolovi.map((sto, i) => <div key={i}>{bull} Sto za {sto.brojOsoba} osobe, slobodno: {sto.slobodnihStolova} </div>)}
      </Typography>
      </CardContent>
      <CardActions>
        <Button href={"/ObjekatUpravljanje/" + props.id} size="small">Više informacija</Button>
        {isAllowed && (
          <Button size="small" onClick={handleDelete}><DeleteIcon /></Button>
        )}
      </CardActions>
    </Card>
  );
}