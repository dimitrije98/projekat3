import React, { useState }from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import FormControl from "@material-ui/core/FormControl";
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
      width: theme.spacing(16),
      height: theme.spacing(16),
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  },
}));

export default function FilterMenu(props) {

    const classes = useStyles();
    const [filter, setFilter] = useState({
        filtriraj: false,
        oznake: [],
        mesta: [],
        stolovi: [],
        filtrirajSlobodnaMesta: false,
        imaSlobodnihMesta: true,
        sortiraj: 0
    });
    const [mestaChecked, setMestaChecked] = useState(Array(props.oznake.length).fill(false));
    const [oznakeChecked, setOznakeChecked] = useState(Array(props.oznake.length).fill(false));
    const [stoloviChecked, setStoloviChecked] = useState(Array(props.stolovi.length).fill(false));
    const [slobodniStoloviChecked, setSlobodniStoloviChecked] = useState("");

    const handleChangeOznaka=(ev, i)=>{
        console.log(props.oznake.length);
        var niz = oznakeChecked.slice();
        niz[i] = ev.target.checked;
        setOznakeChecked(niz);

        if(!filter.filtriraj)
            setFilter({ ...filter, filtriraj: true});

        var oznake = filter.oznake;
        if(ev.target.checked) {
            oznake.push(ev.target.name);
            setFilter({ ...filter, oznake: oznake, filtriraj: true })
        }
        else
        {
            const ind = oznake.indexOf(ev.target.name);
            if(ind > -1) {
                oznake.splice(ind, 1);
                setFilter({ ...filter, oznake: oznake })
            }
        }

        console.log(oznakeChecked);
        console.log(filter.oznake);
    }

    const handleChangeMesto=(ev, i)=>{
        var niz = mestaChecked.slice();
        niz[i] = ev.target.checked;
        setMestaChecked(niz);

        var mesta = filter.mesta;
        if(ev.target.checked) {
            mesta.push(ev.target.name);
            setFilter({ ...filter, mesta: mesta, filtriraj: true })
        }
        else
        {
            const ind = mesta.indexOf(ev.target.name);
            if(ind > -1) {
                mesta.splice(ind, 1);
                setFilter({ ...filter, mesta: mesta })
            }
        }

        console.log(mestaChecked);
        console.log(filter.mesta);
    }

    const handleChangeSto=(ev, i)=>{
        var niz = stoloviChecked.slice();
        niz[i] = ev.target.checked;
        setStoloviChecked(niz);
        console.log(stoloviChecked);

        var stolovi = filter.stolovi;
        if(ev.target.checked) {
            stolovi.push(ev.target.name);
            setFilter({ ...filter, stolovi: stolovi, filtriraj: true })
        }
        else
        {
            const ind = stolovi.indexOf(ev.target.name);
            if(ind > -1) {
                stolovi.splice(ind, 1);
                setFilter({ ...filter, stolovi: stolovi })
            }
        }

        console.log(filter);
    }

    const handleChangeSlobodnaMesta = (ev) => {

        setSlobodniStoloviChecked(ev.target.value);
        var val = (ev.target.value === "true");
        setFilter({ ...filter, filtriraj: true, filtrirajSlobodnaMesta: true, imaSlobodnihMesta: val });
        console.log(filter);
    };

    const handleChangeSortiranje = (ev) => {

        setFilter({ ...filter, filtriraj: true, sortiraj: parseInt(ev.target.value) });
        console.log(filter);
        console.log(ev.target.value);
    };

    const handleSubmit=(ev)=>{
        ev.preventDefault();
        props.uzmiObjekte(filter);
        props.postaviFilter(filter);
        console.log(stoloviChecked);
    }

    return (
        
        <Paper variant="outlined" style={{ margin: 7 }}>
            <Typography gutterBottom variant="h5" component="h1">
                Filter
            </Typography>
            <form id="filteri" onSubmit={handleSubmit}>
                <div id="tip">
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormGroup>
                            <FormLabel component="legend" ><strong>Tip objekta</strong></FormLabel>
                            {props.oznake.map((oznaka, i) => 
                                <FormControlLabel
                                    key={oznaka}
                                    control={<Checkbox checked={oznakeChecked[i]} onChange={(ev) => handleChangeOznaka(ev, i)} name={oznaka} />}
                                    label={oznaka}
                                />)
                            }
                        </FormGroup>
                    </FormControl>
                </div>

                <div id="mesto">
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormGroup>
                            <FormLabel component="legend" ><strong>Mesto</strong></FormLabel>
                            {props.mesta.map((mesto, i) => 
                                <FormControlLabel
                                    key={mesto}
                                    control={<Checkbox checked={mestaChecked[i]} onChange={(ev) => handleChangeMesto(ev, i)} name={mesto} />}
                                    label={mesto}
                                />)
                            }
                        </FormGroup>
                    </FormControl>
                </div>

                <div id="stolovi">
                    <FormControl component="fieldset" className={classes.formControl}>
                        <FormGroup>
                            <FormLabel component="legend" ><strong>Sto za</strong></FormLabel>
                            {props.stolovi.map((sto, i) => 
                                <FormControlLabel
                                    key={sto}
                                    control={<Checkbox checked={stoloviChecked[i]} onChange={(ev) => handleChangeSto(ev, i)} name={sto} />}
                                    label={sto + " osobe"}
                                />)
                            }
                        </FormGroup>
                    </FormControl>
                </div>

                <div id="slobodnaMesta">
                <FormControl component="fieldset" className={classes.formControl}>
                    <FormLabel component="legend"><strong>Ima slobodnih stolova</strong></FormLabel>
                    <RadioGroup aria-label="gender" name="gender1" value={slobodniStoloviChecked} onChange={handleChangeSlobodnaMesta}>
                        <FormControlLabel value="true" control={<Radio />} label="Da" />
                        <FormControlLabel value="false" control={<Radio />} label="Ne" />
                    </RadioGroup>
                    </FormControl>
                </div>

                <div id="sortiranje">
                <FormControl component="fieldset" className={classes.formControl}>
                    <FormLabel component="legend"><strong>Sortiraj po</strong></FormLabel>
                    <RadioGroup aria-label="gender" name="gender1" value={filter.sortiraj} onChange={handleChangeSortiranje}>
                        <FormControlLabel value={0} control={<Radio />} label="Ništa" />
                        <FormControlLabel value={1} control={<Radio />} label="Nazivu A-Z" />
                        <FormControlLabel value={2} control={<Radio />} label="Nazivu Z-A" />
                    </RadioGroup>
                    </FormControl>
                </div>

                <Button variant="outlined" color="primary" type="submit">Prikaži</Button>
            </form>
        </Paper>
        
    );
}