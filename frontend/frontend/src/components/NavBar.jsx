import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';
import AuthService from '../services/auth.service'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  link:{
      color: "white",
  }
}));

export default function NavBar({ user }) {

  const classes = useStyles();
  const isObjekat = user && user.role === "Objekat";
  const isAdmin = user && user.role === "Admin";
  const isLogedIn = isObjekat || isAdmin;
  
  const logOut = () => {
    AuthService.logout();
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Button active="true" color="inherit" href="/"><HomeIcon />Home</Button>
          <Typography variant="h6" className={classes.title}>
            Free Spot
          </Typography>
          {isAdmin && (
            <Button color="inherit" href="/KreirajObjekat">kreiraj objekat</Button>
          )}
          {isLogedIn ? (
            <>
              <Button color="inherit" href={"/ObjekatUpravljanje/" + user.nameid}>Profil</Button>
              <Button color="inherit" href="/Login" onClick={logOut}>Logout</Button>
            </>
          ) : (
            <Button color="inherit" href="/Login">Login</Button>
          )}
          
        </Toolbar>
      </AppBar>
    </div>
  );
}