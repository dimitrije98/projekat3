import React, { useState, useEffect } from "react";
import './App.css';
import objekat from "./components/kreirajObjekat";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import axios from "axios";
import objekti from "./components/objekti";
import objekatUpravljanje from "./components/objekatUpravljanje";
import NavBar from "./components/NavBar";
import login from "./components/login";
import editObjekat from "./components/editObjekat";
import AuthService from './services/auth.service'

axios.defaults.baseURL = "https://localhost:5001/api/";

function App() {

  const [currentUser, setCurrentUser] = useState(undefined);

  useEffect(() => {
    const user = AuthService.getCurrentUser();

    if (user) {
      setCurrentUser(user);
    }
  }, []);


  return (
    <div className="App">
      <NavBar user={currentUser} />
      <Router>
      <Switch>
      <Route path="/Login" component={login} />
        <Route path="/KreirajObjekat" component={objekat} />
        <Route path="/ObjekatUpravljanje/:objekatId" component={objekatUpravljanje} />
        <Route path="/EditObjekat/:objekatId" component={editObjekat} />
        <Route path="/" component={objekti} />
      </Switch>
      </Router>
    </div>
  );
}

export default App;
