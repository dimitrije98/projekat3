import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from "@material-ui/core/InputLabel";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import IconButton from "@material-ui/core/IconButton";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";

//service
import objekatService from '../services/objekat.service'

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    },
    buttonWrapper: {
        display: "flex",
        flexDirection: "row",
    },
    passwordForm: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    form: {
        width: "100%",
        marginTop: theme.spacing(1),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }
  }));

  
export default function EditObjekat(props) {

    let history = useHistory();
    const classes = useStyles();
    const [ime, setIme]=useState('');
    const [grad, setGrad]=useState('');
    const [ulica, setUlica]=useState('');
    const [broj,setBroj]=useState('');
    const [kontakt, setKontakt]=useState('');
    const [username, setUsername]=useState('');
    const [password, setPassword]=useState('');
    const [oznake, setOznake] = useState([]);
    const [stolovi, setStolovi] = useState([
        {
            brojOsoba: 0,
            brStolova: 0,
        }]);
    const [stoloviBr, setStoloviBr] = useState([1]);
    const [showPassword, setShowPassword] = useState(false);

    const vpassword = (value) => {
        if (value.length < 5 || value.length > 40) {
            return false;
        }
        return true;
    };

    useEffect(() => {
        objekatService.uzmiJedanObjekat(props.id)
        .then(res =>{
            var objekat = res.data
            setIme(objekat.ime);
            setGrad(objekat.adresa.grad);
            setUlica(objekat.adresa.ulica);
            setBroj(objekat.adresa.broj);
            setKontakt(objekat.kontakt);
            setUsername(objekat.username);
            setOznake(objekat.oznake);
            setStolovi(objekat.stolovi);
        }).catch(error => {
            console.log(error);
        });

        ValidatorForm.addValidationRule('passwordLengthCheck', vpassword);
 
        return () => {
            ValidatorForm.removeValidationRule('passwordLengthCheck');
        }
    },[props.id])

   
    //handles
    const handleChangeIme=(ev)=>{
       
        setIme(ev.target.value);
        console.log(ime);
    }
    const handleChangeUlica=(ev)=>{
        setUlica(ev.target.value);
        console.log(ulica);
    }
    const handleChangeBroj=(ev)=>{
        setBroj(ev.target.value);
    }
    const handleChangeGrad=(ev)=>{
        setGrad(ev.target.value);
    }

    const handleChangeKontakt=(ev)=>{
        setKontakt(ev.target.value);
    }
     const handleChangeUsername=(ev)=>{
        setUsername(ev.target.value);
    }
     const handleChangePassword=(ev)=>{
        setPassword(ev.target.value);

    }
    const handleChangeOznake=(ev)=>{
        if(ev.target.checked === true)
        {
          setOznake([...oznake, ev.target.name]);
          console.log(oznake);
        }
        else{
            var oznakeFilter= oznake.filter(x => x != [ ev.target.name]);
            setOznake(oznakeFilter);
            console.log(oznake);
        }
    }

    const handleChangeBrojOsoba=(ev, i)=>{
        let niz = stolovi.slice();
        niz[i].brojOsoba = ev.target.value;
        setStolovi(niz);
        console.log("Osobe:")
        console.log(stolovi);

    }

    const handleChangeSto=(ev, i)=>{
        let niz = stolovi.slice();
        niz[i].brStolova = ev.target.value;
        niz[i].slobodnihStolova = ev.target.value;
        setStolovi(niz);
        console.log("Stolovi:")
        console.log(stolovi);
        console.log(stoloviBr);

    }

    const handleNoviSto=(ev)=>{
        const br = stoloviBr[stoloviBr.length - 1] + 1;
        setStoloviBr([...stoloviBr, br]);
        console.log(stoloviBr);
        const newTable = 
        {
            brojOsoba: 0,
            brStolova: 0,
            slobodnihStolova: 0
        };
        setStolovi([...stolovi, newTable]);
        console.log("Dodat novi sto:");
        console.log(stolovi);
    }

    const handleClickShowPassword = () => {
        if (showPassword === false) {
          setShowPassword(true);
        } else setShowPassword(false);
      };

    const handleSubmit=(ev)=>{
        ev.preventDefault();
        
        const objekat={
            id: props.id,
            ime: ime,
            adresa: {grad, ulica, broj},
            kontakt: kontakt,
            username: username,
            oznake: oznake,
            role: "Objekat",
            stolovi: stolovi
        }
        console.log(objekat);

        const objekatDTO={
            ugostiteljskiObjekat: objekat,
            password: password
        }
        console.log(objekatDTO);

        objekatService.azuriraJedanObjekat(objekat)
        .then(res => {
            console.log(res.data);
            history.push("/ObjekatUpravljanje/" + props.id);
            window.location.reload();
        })
        .catch(error =>
        {
            console.log(error);
        })
    }

    const handleSubmitPassword=(ev)=>{
        ev.preventDefault();
        
        const objekatDTO={
            ugostiteljskiObjekat: {id: props.id},
            password: password
        }
        console.log(objekatDTO);

        objekatService.promeniSifruObjektu(objekatDTO)
        .then(res => {
            console.log(res.data);
            history.push("/ObjekatUpravljanje/" + props.id);
            window.location.reload();
        })
        .catch(error =>
        {
            console.log(error);
        })
    }

      
    return (
        <div>
            <ValidatorForm
                className={classes.form}
                onSubmit={handleSubmit}
                onError={errors => console.log(errors)}
            >
                <div>
                    <TextValidator
                        label="Ime objekta"
                        onChange={handleChangeIme}
                        name="ime"
                        value={ime}
                        validators={['required']}
                        errorMessages={['Polje ime je obavezno']}
                    />
                </div>
                <div>< TextField id="standard-basic" style={{ margin: 10 }} label="Ulica " onChange={handleChangeUlica} value={ulica} /> 
                < TextField id="standard-basic" style={{ margin: 10 }} label="Broj " onChange={handleChangeBroj} value={broj}/>
                < TextField id="standard-basic" style={{ margin: 10 }} label="Grad " onChange={handleChangeGrad} value={grad}/></div>
                <div>< TextField id="standard-basic" style={{ margin: 10 }} label="Kontakt " onChange={handleChangeKontakt} value={kontakt}/></div>
                <div>< TextField id="standard-basic" style={{ margin: 10 }}disabled label="username " onChange={handleChangeUsername} value={username}/></div>
                
                <div>
                    <FormControl component="fieldset" className={classes.formControl}>
                    <FormGroup style={{ margin: 10 }}>
                        <FormLabel component="legend"><strong>Izaberite tip objekta</strong></FormLabel>
                        <FormControlLabel
                            control={<Checkbox checked={oznake.findIndex(el => el === 'kafic') >= 0} onChange={handleChangeOznake} name="kafic" />}
                            label="Kafic"
                        />
                        <FormControlLabel
                            control={<Checkbox checked={oznake.findIndex(el => el === 'restoran') >= 0} onChange={handleChangeOznake} name="restoran" />}
                            label="Restoran"
                        />
                        </FormGroup>
                    </FormControl>
                </div>

                <div style={{ margin: 10 }}><InputLabel><strong>Unesite stolove dostupne u kafiću</strong></InputLabel></div>
                <div>
                    {stolovi.map((sto, i) => 
                    <div key={i} className={classes.buttonWrapper} style={{ margin: 15 }}>
                        <InputLabel style={{ margin: 6 }}>Sto za </InputLabel> 
                        <TextField id="standard-basic" placeholder="Broj osoba" type="number" onChange={(ev) => handleChangeBrojOsoba(ev, i)} value={sto.brojOsoba}/>
                        <InputLabel style={{ margin: 6 }}>osoba - komada</InputLabel>
                        <TextField id="standard-basic" placeholder="Broj stolova" type="number" onChange={(ev) => handleChangeSto(ev, i)} value={sto.brStolova}/>

                    </div>)
                    }
                    <div><Button color="primary" onClick={handleNoviSto}>+ Novi tip stola</Button></div>
                </div>

                <Button variant="contained" color="primary" type="submit">
                    Izmeni Objekat
                </Button>
            </ValidatorForm>

            <ValidatorForm
                className={classes.passwordForm}
                style={{ margin: 30 }}
                onSubmit={handleSubmitPassword}
                onError={errors => console.log(errors)}
            >
                <div className={classes.buttonWrapper}>
                    <TextValidator
                        label="Password"
                        style={{ margin: 10 }}
                        fullWidth
                        onChange={handleChangePassword}
                        name="password"
                        value={password}
                        type={showPassword ? "text" : "password"}
                        validators={['required', 'passwordLengthCheck']}
                        errorMessages={['Šifra je obavezna', 'Šifra mora biti između 5 i 40 karaktera']}
                    />
                    <IconButton
                        aria-label="toggle password visibility"
                        dge="end"
                        className={classes.button}
                        onClick={handleClickShowPassword}
                    >
                    {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                    </IconButton>
                </div>
                <Button variant="contained" color="primary" type="submit">
                    Promeni šifru
                </Button>
            </ValidatorForm>
        </div>
    )
}
