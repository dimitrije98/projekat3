import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import ObjekatCard from '../components/ObjekatCard';
import Grid from '@material-ui/core/Grid';
import ObjekatService from '../services/objekat.service';
import AuthService from '../services/auth.service'
import FilterMenu from '../components/FilterMenu';

const useStyles = makeStyles({
    root: {
        display: "flex",
        flexDirection: "row"
    },
  });

export default function Objekti() {

    const classes = useStyles();
    const [objekti, setObjekti] = useState([]);
    const [currentUser, setCurrentUser] = useState(undefined);
    const [sadrzajFilter, setSadrzajFilter] = useState({oznake: [], mesta: [], stolovi: []});
    const [filteri, setFilteri] = useState({filtriraj: false});


    useEffect(() => {
        ObjekatService.uzmiSveObjekte({})
            .then(res =>{
                var objekti = res.data
                setObjekti(objekti);
            }).catch(function (error) {
                console.log(error);
            });
        
        ObjekatService.uzmiSadrzajFiltera()
        .then(res =>{
            var filter = res.data;
            setSadrzajFilter(filter);
            console.log(filter);
        }).catch(function (error) {
            console.log(error);
        });

        const user = AuthService.getCurrentUser();

        if (user) {
            setCurrentUser(user);
        }
    
        return () => {};
      }, []);

    function getObjekti(filter = filteri) {
        ObjekatService.uzmiSveObjekte(filter)
            .then(res =>{
                var objekti = res.data
                setObjekti(objekti);
            }).catch(function (error) {
                console.log(error);
            });
    }

    return (
        <div className={classes.root}>
            <FilterMenu oznake={sadrzajFilter.oznake} mesta={sadrzajFilter.mesta} stolovi={sadrzajFilter.stolovi} uzmiObjekte={getObjekti} postaviFilter={setFilteri}/>

            <Grid container spacing={1}>
                {objekti.map(objekat => <ObjekatCard key={objekat.id} user={currentUser} id={objekat.id} ime={objekat.ime} adresa={objekat.adresa} oznake={objekat.oznake} stolovi={objekat.stolovi} uzmiObjekte={getObjekti}/>)}
            </Grid>
        </div>
    )
}