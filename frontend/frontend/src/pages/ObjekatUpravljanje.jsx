import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom";
import 'date-fns';

import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import ContactPhoneIcon from '@material-ui/icons/ContactPhone';
import LocalBarIcon from '@material-ui/icons/LocalBar';
import EventSeatSharpIcon from '@material-ui/icons/EventSeatSharp';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  DateTimePicker,
  DatePicker
} from '@material-ui/pickers';
import EditIcon from '@material-ui/icons/Edit';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import DogadjajCard from '../components/DogadjajCard';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import authService from '../services/auth.service'
import objekatService from '../services/objekat.service';
import dogadjajService from '../services/dogadjaj.service';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      maxWidth: 752,
    },
    demo: {
      backgroundColor: theme.palette.background.paper,
    },
    title: {
      margin: theme.spacing(4, 0, 2),
    },
    form: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    naslov: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'nowrap',
    },
    filter: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'baseline',
        flexWrap: 'nowrap',
      },
    rootGridList: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: "100%",
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },
  }));

export default function ObjekatUpravljanje(props) {
    
    const initialDogadjaj = {
        naziv: "",
        vremeOdrzavanja: new Date(),
        opis: "",
        ugostiteljskiObjekat: props.id
    };

    let history = useHistory();
    const classes = useStyles();
    const [objekat, setObjekat] = useState({adresa: {ulica: "", broj: "", grad: ""}, oznake: [], stolovi: []});
    const [noviDogadjaj, setNoviDogadjaj] = useState(initialDogadjaj);
    const [dogadjaji, setDogadjaji] = useState([]);
    const [currentUser, setCurrentUser] = useState(undefined);
    const [filterDate, setFilterDate] = useState({filtriraj: false, filter: new Date()});
    const [ponistiDisabled, setPonistiDisabled] = useState(true);

    const isOwner = currentUser && currentUser.role === "Objekat" && currentUser.nameid === props.id;
    const isAdmin = currentUser && currentUser.role === "Admin";
    const isAdminOrOwner = isOwner || isAdmin;
    const isAdminProfile = currentUser && currentUser.role === "Admin" && currentUser.nameid === props.id;
        
    // Ako ne psotoji objekat koji trazi puca aplikacija
    useEffect(() => {
        objekatService.uzmiJedanObjekat(props.id)
            .then(res =>{
                var objekat = res.data
                setObjekat(objekat);
                console.log(objekat);
            }).catch(function (error) {
                console.log(error);
            });
        
            dogadjajService.uzmiSveDogadjaje(props.id, false, filterDate.filter.toISOString())
            .then(res =>{
                var dogadjaji = res.data
                setDogadjaji(dogadjaji);
                console.log(dogadjaji);
            }).catch(error => {
                console.log(error);
            });

        const user = authService.getCurrentUser();

        if (user) {
            setCurrentUser(user);
        }
    
        return () => {};
      }, [props.id, filterDate.filter, filterDate.filtriraj]);

    const handleDelete=(ev)=>{
        objekatService.obrisiObjekat(objekat.id)
        .then(res=>{
            console.log(res);
            console.log(res.data);
            if(isOwner)
                authService.logout();
            history.push('/');
            window.location.reload();
        }).catch(error => {
            console.log(error);
        });
    }

    const handlePromeniStanjeStolova=(i, val)=>{
        console.log(objekat.stolovi[i].slobodnihStolova);
        var novoStanje = objekat.stolovi[i].slobodnihStolova + val;
        if(novoStanje >= 0 && novoStanje <= objekat.stolovi[i].brStolova) {

            let niz = objekat.stolovi.slice();
            niz[i].slobodnihStolova = novoStanje;
            objekatService.azurirajSobodneStolove(objekat.id, niz)
            .then(res=>{
                console.log(res.data);
                
                setObjekat({ ...objekat, stolovi: niz });
                console.log("Promenjen sto:")
                console.log(objekat.stolovi);
            }).catch(error => {
                console.log(error);
            });
            
        }
    }

    const handleDateChange = (date) => {
        setNoviDogadjaj({...noviDogadjaj, vremeOdrzavanja: date});
    };
    
    const handleChangeDogadjaj=(ev)=>{
        setNoviDogadjaj({ ...noviDogadjaj, [ev.target.name]: ev.target.value})
    }

    function getDogadjaji() {
        console.log(filterDate);
        dogadjajService.uzmiSveDogadjaje(objekat.id, filterDate.filtriraj, filterDate.filter.toISOString())
                .then(res =>{
                    var dogadjaji = res.data
                    setDogadjaji(dogadjaji);
                    console.log(dogadjaji);
                }).catch(error => {
                    console.log(error);
                });
    }

    const handleFilterDateChange = (date) => {
        console.log(filterDate);
        setFilterDate({ ...filterDate, filtriraj: true, filter: date});
        setPonistiDisabled(false);
        getDogadjaji();
    };

    const handlePonistiFilter = () => {
        setFilterDate({ ...filterDate, filtriraj: false});
        setPonistiDisabled(true);
    };

    const handleDogadjajSubmit=(ev)=>{
        ev.preventDefault();

        dogadjajService.kreirajDogadjaj(noviDogadjaj)
        .then(res=>{
            console.log(res.data);
            setNoviDogadjaj(initialDogadjaj);

            getDogadjaji();
        }).catch(error => {
            console.log(error);
        });
    }

    return (
        <div>
            <div className={classes.naslov}>
            <div>
                {isOwner && (
                <Button href={"/EditObjekat/"+props.id} variant="outlined" style={{ margin: 8 }} color="primary" >
                    <EditIcon />Edit
                </Button>
                )}
            </div>
            <h1>{objekat.ime}</h1>
            <div>
                {isAdminOrOwner && (
                    <Button variant="outlined" color="secondary" style={{ margin: 8 }} onClick={handleDelete}>
                        <DeleteIcon />Obriši
                    </Button>
                )}
            </div>
        </div>
            <Grid item xs={12} md={6}>
            <Typography variant="h5" className={classes.title}>
                Osnovne informacije
            </Typography>
            <div className={classes.demo}>
                <List >
                    {!isAdminProfile ? (
                        <ListItem>
                            <ListItemIcon>
                                <LocalBarIcon />
                            </ListItemIcon>
                            <ListItemText
                                primary={objekat.oznake.map((oznaka) => {return (oznaka + ", ")})}
                            />
                        </ListItem>
                    ) : (
                        <>
                            <ListItem>
                                <ListItemIcon>
                                    <VerifiedUserIcon />
                                </ListItemIcon>
                                <ListItemText
                                    primary={"Admin"}
                                />
                            </ListItem>
                        </>
                    )}
                    
                    <ListItem>
                        <ListItemIcon>
                            <HomeWorkIcon />
                        </ListItemIcon>
                        <ListItemText
                            primary={objekat.adresa.ulica + " " + objekat.adresa.broj + ", " + objekat.adresa.grad}
                        />
                    </ListItem>
                    <ListItem>
                        <ListItemIcon>
                            <ContactPhoneIcon />
                        </ListItemIcon>
                        <ListItemText
                            primary={objekat.kontakt}
                        />
                    </ListItem>
                </List>
            </div>
            </Grid>
            
            {!isAdminProfile && (
                <>
                    <Grid item xs={12} md={6}>
                    <Typography variant="h5" className={classes.title}>
                        Slobodni stolovi
                    </Typography>
                    <div className={classes.demo}>
                        <List>
                        {objekat.stolovi.map((sto, i) => {
                            var text = "Sto za " + sto.brojOsoba + " osobe - dostupno " + sto.slobodnihStolova + " / " + sto.brStolova;
                            return (
                                <ListItem key={i}>
                                    <ListItemAvatar>
                                    <Avatar>
                                        <EventSeatSharpIcon />
                                    </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText
                                    primary={text}
                                    />
                                    {isOwner && (
                                        <>
                                            <ListItemSecondaryAction>
                                                <IconButton edge="end" onClick={() => handlePromeniStanjeStolova(i, 1)}>
                                                    <AddIcon />
                                                </IconButton>
                                                <IconButton edge="end" onClick={() => handlePromeniStanjeStolova(i, -1)}>
                                                    <RemoveIcon />
                                                </IconButton>
                                            </ListItemSecondaryAction>
                                        </>
                                    )}
                                    
                                </ListItem>)
                            })
                        }
                        </List>
                    </div>
                    </Grid>

                    <div>
                        <Typography variant="h5" className={classes.title}>
                            Događaji
                        </Typography>
                        {isOwner && (
                            <>
                                <form className={classes.form} noValidate autoComplete="off" onSubmit={handleDogadjajSubmit}>
                                    <TextField id="outlined-basic" name="naziv" style={{ margin: 8 }} label="Naziv" variant="outlined" value={noviDogadjaj.naziv} onChange={handleChangeDogadjaj} />
                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                        <DateTimePicker
                                            variant="inline"
                                            inputVariant="outlined"
                                            style={{ margin: 8 }}
                                            ampm={false}
                                            label="Datum održavanja"
                                            value={noviDogadjaj.vremeOdrzavanja}
                                            onChange={handleDateChange}
                                            onError={console.log}
                                            disablePast
                                            format="yyyy/MM/dd HH:mm"
                                        />
                                    </MuiPickersUtilsProvider>
                                    <TextField 
                                        id="outlined-full-width" 
                                        style={{ margin: 8 }} 
                                        fullWidth 
                                        margin="normal" 
                                        multiline 
                                        rows={2} 
                                        name="opis" 
                                        label="Opis" 
                                        variant="outlined" 
                                        value={noviDogadjaj.opis}
                                        onChange={handleChangeDogadjaj} 
                                    />
                                    <Button variant="outlined" color="primary" style={{ margin: 8 }} type="submit">
                                        <CreateIcon />Kreiraj događaj
                                    </Button>
                                </form>
                            </>
                        )}

                        <div id="filter" className={classes.filter}>
                            <Typography variant="h6" component="h6" style={{ margin: 15 }}>
                                Prikaži događaje za datum:
                            </Typography>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DatePicker
                                    variant="inline"
                                    inputVariant="outlined"
                                    style={{ margin: 15 }}
                                    ampm={false}
                                    label="Datum održavanja"
                                    value={filterDate.filter}
                                    onChange={handleFilterDateChange}
                                    onError={console.log}
                                    format="yyyy/MM/dd"
                                />
                            </MuiPickersUtilsProvider>
                            <Button variant="outlined" style={{ margin: 15 }} color="secondary" disabled={ponistiDisabled} onClick={handlePonistiFilter}>x</Button>
                        </div>

                        <div className={classes.rootGridList}>
                            <GridList className={classes.gridList} cols={3.5}>
                                {dogadjaji.map((dogadjaj) => (
                                <GridListTile key={dogadjaj.id}>
                                    <DogadjajCard user={currentUser} objekatId={objekat.id} id={dogadjaj.id} naziv={dogadjaj.naziv} vremeOdrzavanja={new Date(dogadjaj.vremeOdrzavanja)} opis={dogadjaj.opis} uzmiDogadjaje={() => getDogadjaji()} />
                                </GridListTile>
                                ))}
                            </GridList>
                        </div>
                        
                    </div>
                </>
            )}
            
        </div>
    )
}