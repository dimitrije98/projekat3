import React, { useState, useEffect } from 'react'
import ObjekatService from "../services/objekat.service";

//mui
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from "@material-ui/core/InputLabel";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import IconButton from "@material-ui/core/IconButton";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    },
    buttonWrapper: {
        display: "flex",
        flexDirection: "row",
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    root: {
        width: '100%',
        '& > * + *': {
          marginTop: theme.spacing(2),
        },
    },
  }));


  
  export default function KreirajObjekat() {
    
    const classes = useStyles();
    const [ime, setIme]=useState('');
    const [grad, setGrad]=useState('');
    const [ulica, setUlica]=useState('');
    const [broj,setBroj]=useState('');
    const [kontakt, setKontakt]=useState('');
    const [username, setUsername]=useState('');
    const [password, setPassword]=useState('');
    const [oznake, setOznake] = useState({
        kafic: false,
        restoran: false
      });
    const [stolovi, setStolovi] = useState([
        {
            brojOsoba: 0,
            brStolova: 0,
            slobodnihStolova: 0
        }]);
    const [stoloviBr, setStoloviBr] = useState([1]);
    const [showPassword, setShowPassword] = useState(false);
    const [message, setMessage] = React.useState({open: false, type: "succes", content: ""});


    const vusername = (value) => {
        if (value.length < 3 || value.length > 20) {
          return false;
        }
        return true;
      };
      
    const vpassword = (value) => {
        if (value.length < 5 || value.length > 40) {
            return false;
        }
        return true;
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setMessage({ ...message, open: false });
    };

    useEffect(() => {

        ValidatorForm.addValidationRule('usernameLengthCheck', vusername);
        ValidatorForm.addValidationRule('passwordLengthCheck', vpassword);

      
        // returned function will be called on component unmount 
        return () => {
          ValidatorForm.removeValidationRule('usernameLengthCheck');
          ValidatorForm.removeValidationRule('passwordLengthCheck');
        }
      }, [])

    const handleChangeIme=(ev)=>{
        setIme(ev.target.value);
    }
    const handleChangeUlica=(ev)=>{
        setUlica(ev.target.value);
    }
    const handleChangeBroj=(ev)=>{
        setBroj(ev.target.value);
    }
    const handleChangeGrad=(ev)=>{
        setGrad(ev.target.value);
    }

    const handleChangeKontakt=(ev)=>{
        setKontakt(ev.target.value);
    }
     const handleChangeUsername=(ev)=>{
        setUsername(ev.target.value);
    }
     const handleChangePassword=(ev)=>{
        setPassword(ev.target.value);
    }
    const handleChangeOznake=(ev)=>{
        setOznake({ ...oznake, [ev.target.name]: ev.target.checked });
    }

    const handleChangeBrojOsoba=(ev, i)=>{
        let niz = stolovi.slice();
        niz[i].brojOsoba = ev.target.value;
        setStolovi(niz);
        console.log("Osobe:")
        console.log(stolovi);

    }

    const handleChangeSto=(ev, i)=>{
        let niz = stolovi.slice();
        niz[i].brStolova = ev.target.value;
        niz[i].slobodnihStolova = ev.target.value;
        setStolovi(niz);
        console.log("Stolovi:")
        console.log(stolovi);
        console.log(stoloviBr);

    }

    const handleNoviSto=(ev)=>{
        const br = stoloviBr[stoloviBr.length - 1] + 1;
        setStoloviBr([...stoloviBr, br]);
        console.log(stoloviBr);
        const newTable = 
        {
            brojOsoba: 0,
            brStolova: 0,
            slobodnihStolova: 0
        };
        setStolovi([...stolovi, newTable]);
        console.log("Dodat novi sto:");
        console.log(stolovi);
    }

    const handleClickShowPassword = () => {
        if (showPassword === false) {
          setShowPassword(true);
        } else setShowPassword(false);
      };

    const handleSubmit=(ev)=>{
        ev.preventDefault();

        const ozn = [];
        if(oznake.kafic){
            ozn.push('kafic');
        }
        if(oznake.restoran){
            ozn.push('restoran');
        }

        const objekatDTO = {
            ugostiteljskiObjekat: {
                ime: ime,
                adresa: {grad, ulica, broj},
                kontakt: kontakt,
                username: username,
                role: "Objekat",
                oznake: ozn,
                stolovi: stolovi,
                dogadjaji: []
            },
            password: password
        };

        ObjekatService.kreirajObjekat(objekatDTO)
        .then(response => {
            console.log(response);
            setMessage({ open: true, type: "success", content: [response.data]});
            setIme("");
            setUlica("");
            setBroj("");
            setGrad("");
            setKontakt("");
            setUsername("");
            setPassword("");
            setOznake({
                kafic: false,
                restoran: false
            });
            setStolovi([
                {
                    brojOsoba: 0,
                    brStolova: 0,
                    slobodnihStolova: 0
                }
            ]);
            setStoloviBr([1]);
            //window.location.reload();
            })
            .catch(error => {
                console.log(error.response);
                setMessage({ open: true, type: "error", content: [error.response.data]});
            });
    }
    
      return (
          <div>
                <ValidatorForm
                    className={classes.form}
                    onSubmit={handleSubmit}
                    onError={errors => console.log(errors)}
                >
                    <div>
                    <TextValidator
                        label="Ime objekta"
                        onChange={handleChangeIme}
                        name="ime"
                        value={ime}
                        validators={['required']}
                        errorMessages={['Polje ime je obavezno']}
                    />
                    </div>
                    <div>< TextField id="standard-basic" style={{ margin: 10 }} label="Ulica" value={ulica} onChange={handleChangeUlica} /> 
                    < TextField id="standard-basic" style={{ margin: 10 }} label="Broj" value={broj} onChange={handleChangeBroj} />
                    < TextField id="standard-basic" style={{ margin: 10 }} label="Grad" value={grad} onChange={handleChangeGrad} /></div>
                    <div>< TextField id="standard-basic" label="Kontakt" value={kontakt} onChange={handleChangeKontakt} /></div>
                    <div>
                    <TextValidator
                        label="Username"
                        style={{ margin: 10 }}
                        onChange={handleChangeUsername}
                        name="username"
                        value={username}
                        validators={['required', 'usernameLengthCheck']}
                        errorMessages={['Polje username je obavezno', 'Username mora biti između 3 i 20 karaktera']}
                    />
                    </div>
                    <div className={classes.buttonWrapper}>
                              <TextValidator
                                  label="Password"
                                  
                                  style={{ margin: 10 }}
                                  fullWidth
                                  onChange={handleChangePassword}
                                  name="password"
                                  value={password}
                                  type={showPassword ? "text" : "password"}
                                  validators={['required', 'passwordLengthCheck']}
                                  errorMessages={['Šifra je obavezna', 'Šifra mora biti između 5 i 40 karaktera']}
                              />
                              <IconButton
                                aria-label="toggle password visibility"
                                dge="end"
                                className={classes.button}
                                onClick={handleClickShowPassword}
                              >
                                {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                              </IconButton>
                            </div>

                    <div>
                        <FormControl component="fieldset" className={classes.formControl}>
                        <FormGroup>
                            <FormLabel component="legend" ><strong>Izaberite tip objekta</strong></FormLabel>
                            <FormControlLabel
                                control={<Checkbox checked={oznake.kafic} onChange={handleChangeOznake} name="kafic" />}
                                label="Kafic"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={oznake.restoran} onChange={handleChangeOznake} name="restoran" />}
                                label="Restoran"
                            />
                            </FormGroup>
                        </FormControl>
                    </div>

                    <div>
                    <div><InputLabel><strong>Unesite stolove dostupne u kafiću</strong></InputLabel></div>
                    <div>
                        {stoloviBr.map((sto, i) => 
                        <div className={classes.buttonWrapper} style={{ margin: 15 }}>
                            <InputLabel style={{ margin: 6 }}>Sto za </InputLabel> 
                            <TextField id="standard-basic" placeholder="Broj osoba" type="number" onChange={(ev) => handleChangeBrojOsoba(ev, i) } />
                            <InputLabel style={{ margin: 6 }}>osoba - komada</InputLabel>
                            <TextField id="standard-basic" placeholder="Broj stolova" type="number" onChange={(ev) => handleChangeSto(ev, i)} />
                        </div>)
                        }
                        <div><Button color="primary" onClick={handleNoviSto}>+ Novi tip stola</Button></div>
                        </div>
                    </div>
                    
                    
                    <Button variant="contained" color="primary" type="submit">
                        Dodaj Objekat
                    </Button>
                </ValidatorForm>
                <div className={classes.root}>
                    <Snackbar open={message.open} autoHideDuration={6000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity={message.type}>
                            {message.content}
                        </Alert>
                    </Snackbar>
                </div>
          </div>
      )
  }
  