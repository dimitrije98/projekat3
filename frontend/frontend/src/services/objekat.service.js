
import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "https://localhost:5001/api/UgostiteljskiObjekat/";

const uzmiSadrzajFiltera = () => {
  return axios.get(API_URL + "UzmiSadrzajFiltera");
};

const uzmiSveObjekte = (filter) => {
  return axios.post(API_URL + "UzmiUgostiteljskeObjekte", filter);
};

const uzmiJedanObjekat = (id) => {
  return axios.get(API_URL + "UzmiUgostiteljskiObjekat/" + id);
};

const kreirajObjekat = (objekatDTO) => {
  return axios.post(API_URL + "KreirajUgostiteljskiObjekat", objekatDTO, { headers: authHeader() });
};

const obrisiObjekat = (id) => {
  return axios.delete(API_URL + "ObrisiUgostiteljskiObjekat/" + id, { headers: authHeader() });
}

const azurirajSobodneStolove = (id, stolovi) => {
  return axios.put(API_URL + "IzmeniBrojSlobodnihStolova/" + id, stolovi, { headers: authHeader() });
}

const azuriraJedanObjekat =(objekat)=>{
  return axios.put(API_URL + "IzmeniUgostiteljskiObjekat/" , objekat, { headers: authHeader() });
}

const promeniSifruObjektu =(objekatDTO)=>{
  return axios.put(API_URL + "PromeniSifru/" , objekatDTO, { headers: authHeader() });
}

export default {
  uzmiSadrzajFiltera,
  uzmiSveObjekte,
  uzmiJedanObjekat,
  kreirajObjekat,
  obrisiObjekat,
  azurirajSobodneStolove,
  azuriraJedanObjekat,
  promeniSifruObjektu
};
