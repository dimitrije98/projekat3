import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "https://localhost:5001/api/Dogadjaj/";

const uzmiSveDogadjaje = (idObjekta, filtriraj, datum) => {
    return axios.get(API_URL + 'UzmiDogadjaje/' + idObjekta + '/' + filtriraj + '/' + datum);
};

const kreirajDogadjaj = (dogadjaj) => {
  return axios.post(API_URL + "KreirajDogadjaj", dogadjaj, { headers: authHeader() });
};

const obrisiDogadjaj = (id) => {
  return axios.delete(API_URL + "ObrisiDogadjaj/" + id, { headers: authHeader() });
}



export default {
  uzmiSveDogadjaje,
  kreirajDogadjaj,
  obrisiDogadjaj,
};