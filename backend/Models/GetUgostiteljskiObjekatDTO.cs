using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace backend.Models
{
    public class GetUgostiteljskiObjekatDTO
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string ime { get; set; }

        public Adresa adresa { get; set; }

        public string kontakt { get; set; }

        public string username { get; set; }

        public List<string> oznake { get; set; }

        public List<Stolovi> stolovi { get; set; }

    }
}