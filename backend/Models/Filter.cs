using System.Collections.Generic;

namespace backend.Models
{
    public enum Sort : ushort
    {
        None,
        NazivAZ,
        NazivZA
    }
    
    public class Filter
    {
        public bool filtriraj { get; set; }

        public List<string> oznake { get; set; }
        
        public List<string> mesta { get; set; }

        public List<int> stolovi { get; set; }

        public bool filtrirajSlobodnaMesta { get; set; }
        public bool imaSlobodnihMesta { get; set; }

        public Sort sortiraj { get; set; }

    }
}