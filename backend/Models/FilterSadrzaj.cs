using System.Collections.Generic;

namespace backend.Models
{
    public class FilterSadrzaj
    {
         public List<string> oznake { get; set; }
        public List<string> mesta { get; set; }
        public List<int> stolovi { get; set; }

    }
}