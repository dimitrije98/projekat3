using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace backend.Models
{
    public class Stolovi
    {
        public int brojOsoba { get; set; }

        public int brStolova { get; set; }

        public int slobodnihStolova { get; set; }

    }
}