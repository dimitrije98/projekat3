using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System;

namespace backend.Models
{
    public class Dogadjaj
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string naziv { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime vremeOdrzavanja { get; set; }

        public string opis { get; set; }

        [BsonRepresentation(BsonType.ObjectId)] 
        public string ugostiteljskiObjekat { get; set; }

    }
}