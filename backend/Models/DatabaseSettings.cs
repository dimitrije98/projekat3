namespace backend.Models
{
    public class DatabaseSettings : IDatabaseSettings
    {
        public string UgostiteljskiObjekatCollectionName { get; set; }
        public string DogadjajiCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IDatabaseSettings
    {
        string UgostiteljskiObjekatCollectionName { get; set; }
        string DogadjajiCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}