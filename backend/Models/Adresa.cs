using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace backend.Models
{
    public class Adresa
    {
        public string grad { get; set; }

        public string ulica { get; set; }

        public string broj { get; set; }

    }
}