using System.Threading.Tasks;
using backend.Models;
using MongoDB.Driver;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;

namespace backend.Services
{
    public class AuthRepository : IAuthRepository
    {
        private readonly IMongoCollection<UgostiteljskiObjekat> _objekti;
        private readonly IConfiguration _configuration;

        public AuthRepository(IDatabaseSettings settings, IConfiguration configuration)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _objekti = database.GetCollection<UgostiteljskiObjekat>(settings.UgostiteljskiObjekatCollectionName);

            _configuration = configuration;
        }

        public async Task<string> Login(string username, string password)
        {
            UgostiteljskiObjekat objekat = await _objekti.Find(o => o.username.ToLower() == username.ToLower()).FirstOrDefaultAsync();
            if(objekat == null)
                return null;
            else if(!VerifyPasswordHash(password, objekat.passwordHash, objekat.passwordSalt))
                return null;
            else return CreateToken(objekat);
        }

        public async Task<string> Register(UgostiteljskiObjekat objekat, string password)
        {
            if(await UserExists(objekat.username))
                return null;
            
            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            objekat.passwordHash = passwordHash;
            objekat.passwordSalt = passwordSalt;

            await _objekti.InsertOneAsync(objekat);
            return objekat.Id;
        }

        public async Task<string> Update(string id, string password)
        {
            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            var update = Builders<UgostiteljskiObjekat>.Update.Set(obj => obj.passwordHash, passwordHash);
            await _objekti.UpdateOneAsync(objekat => objekat.Id == id, update);

            update = Builders<UgostiteljskiObjekat>.Update.Set(obj => obj.passwordSalt, passwordSalt);
            await _objekti.UpdateOneAsync(objekat => objekat.Id == id, update);
            
            return id;
        }

        public async Task<bool> UserExists(string username)
        {
            var objekat = await _objekti.Find(obj => obj.username.ToLower() == username.ToLower()).FirstOrDefaultAsync();
            return objekat != null;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using(var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using(var hmac = new HMACSHA512(passwordSalt))
            {
                byte[] computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                for(int i = 0; i < computedHash.Length; i++)
                    if(computedHash[i] != passwordHash[i])
                        return false;
                return true;
            }
        }

        private string CreateToken(UgostiteljskiObjekat objekat)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, objekat.Id),
                new Claim(ClaimTypes.Name, objekat.username),
                new Claim(ClaimTypes.Role, objekat.role)
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8
                                                        .GetBytes(_configuration.GetSection("AppSettings:Token").Value));

            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }

    public interface IAuthRepository
    {
        Task<string> Register(UgostiteljskiObjekat objekat, string password);
        Task<string> Login(string username, string password);
        Task<string> Update(string id, string password);
        Task<bool> UserExists(string username);

    }
}