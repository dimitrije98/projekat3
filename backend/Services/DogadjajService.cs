using backend.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace backend.Services
{
    public class DogadjajService
    {
        private readonly IMongoCollection<Dogadjaj> _dogadjaji;

        public DogadjajService(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _dogadjaji = database.GetCollection<Dogadjaj>(settings.DogadjajiCollectionName);
        }

        public async Task<Dogadjaj> CreateAsync(Dogadjaj dogadjaj)
        {
            await _dogadjaji.InsertOneAsync(dogadjaj);
            return dogadjaj;
        }

        public async Task<List<Dogadjaj>> GetByFilter(FilterDefinition<Dogadjaj> filter, SortDefinition<Dogadjaj> sort)
        {
            return await _dogadjaji.Find(filter).Sort(sort).ToListAsync();
        }

        public async Task<Dogadjaj> GetById(string id)
        {
            return await _dogadjaji.Find(dogadjaj => dogadjaj.Id == id).FirstOrDefaultAsync();
        }

        public async Task UpdateOne(Dogadjaj dogadjaj)
        {
            await _dogadjaji.ReplaceOneAsync(dog => dog.Id == dogadjaj.Id, dogadjaj);
        }

        public async Task DeleteOne(string id)
        {
            await _dogadjaji.DeleteOneAsync(dogadjaj => dogadjaj.Id == id);
        }
    }
}