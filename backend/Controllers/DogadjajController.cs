using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using backend.Models;
using MongoDB.Driver;
using backend.Services;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DogadjajController : ControllerBase
    {

        private readonly DogadjajService _dogadjajService;
        private readonly IMongoCollection<UgostiteljskiObjekat> _objekti;

        public DogadjajController(DogadjajService service, IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _objekti = database.GetCollection<UgostiteljskiObjekat>(settings.UgostiteljskiObjekatCollectionName);
            _dogadjajService = service;
        }

        [Authorize(Roles = "Objekat")]
        [HttpPost]
        [Route("KreirajDogadjaj")]
        public async Task<IActionResult> CreateDogadjaj(Dogadjaj dogadjaj)
        {
            string authenticatedId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            if(authenticatedId != dogadjaj.ugostiteljskiObjekat)
                return StatusCode(403, "You are not allowed to create Dogadjaj for other users!");

            UgostiteljskiObjekat obj = await _objekti.Find(objekat => objekat.Id == dogadjaj.ugostiteljskiObjekat).FirstOrDefaultAsync();
            if(obj == null)
            {
                return NotFound("Objekat for which you want to add Dogadjaj doesn't exist in the database!");
            }

            Dogadjaj result = await _dogadjajService.CreateAsync(dogadjaj);
            var updateObjekat = Builders<UgostiteljskiObjekat>.Update.AddToSet(objekat => objekat.dogadjaji, result.Id);
            await _objekti.UpdateOneAsync(objekat => objekat.Id == result.ugostiteljskiObjekat, updateObjekat);

            return Ok("Document dogadjaj id:" + result.Id + "has been created in the database");
        }

        [HttpGet]
        [Route("UzmiDogadjaje/{objekatId}/{filtriraj}/{datum}")]
        public async Task<List<Dogadjaj>> GetDogadjaji(string objekatId, bool filtriraj, DateTime datum = new DateTime())
        {
            List<FilterDefinition<Dogadjaj>> filteri = new List<FilterDefinition<Dogadjaj>>();
            var builder = Builders<Dogadjaj>.Filter;
            filteri.Add(builder.Eq(dogadjaj => dogadjaj.ugostiteljskiObjekat, objekatId));
            if(filtriraj)
            {
                filteri.Add(builder.Gte(dogadjaj => dogadjaj.vremeOdrzavanja, datum.Date));
                var end = datum.Date.AddDays(1);
                filteri.Add(builder.Lt(dogadjaj => dogadjaj.vremeOdrzavanja, end));
            }
                
            var query = builder.And(filteri);
                        
            var sortBuilder = Builders<Dogadjaj>.Sort;
            SortDefinition<Dogadjaj> sort = sortBuilder.Descending(dogadjaj => dogadjaj.vremeOdrzavanja);

            return await _dogadjajService.GetByFilter(query, sort);
        }

        [Authorize(Roles = "Objekat,Admin")]
        [HttpDelete]
        [Route("ObrisiDogadjaj/{id}")]
        public async Task<IActionResult> DeleteDogadjaj(string id)
        {
            Dogadjaj dogadjaj = await _dogadjajService.GetById(id);
            if(dogadjaj == null)
                return NotFound("Requsted document doesn't exist in the database!");
            
            string authenticatedRole = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
            if(authenticatedRole == "Objekat")
            {
                string authenticatedId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
                if(authenticatedId != dogadjaj.ugostiteljskiObjekat)
                    return StatusCode(403, "You are not allowed to delete other user's data!");
            } 
            else
            {
                UgostiteljskiObjekat obj = await _objekti.Find(objekat => objekat.Id == dogadjaj.ugostiteljskiObjekat).FirstOrDefaultAsync();
                if(obj == null)
                {
                    return NotFound("Parent Objekat doesn't exist for requsted Dogadjaj in the database!");
                }
            }

            var updateObjekat = Builders<UgostiteljskiObjekat>.Update.Pull(objekat => objekat.dogadjaji, dogadjaj.Id);
            await _objekti.UpdateOneAsync(objekat => objekat.Id == dogadjaj.ugostiteljskiObjekat, updateObjekat);

            await _dogadjajService.DeleteOne(id);
            return Ok("Documnet dogadjaj id: " + id + "has been deleted in the databse!");
        }
    }
}
