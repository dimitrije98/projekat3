using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using backend.Models;
using MongoDB.Driver;
using backend.Services;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using AutoMapper;
using MongoDB.Driver.Linq;

namespace backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UgostiteljskiObjekatController : ControllerBase
    {

        private readonly IMongoCollection<UgostiteljskiObjekat> _objekti;
        private readonly IMongoCollection<Dogadjaj> _dogadjaji;
        private readonly IAuthRepository _authRepository;
        private readonly IMapper _mapper;

        public UgostiteljskiObjekatController(IDatabaseSettings settings, IAuthRepository authRepository, IMapper mapper)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _objekti = database.GetCollection<UgostiteljskiObjekat>(settings.UgostiteljskiObjekatCollectionName);
            _dogadjaji = database.GetCollection<Dogadjaj>(settings.DogadjajiCollectionName);
            
            _authRepository = authRepository;
            _mapper = mapper;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("KreirajUgostiteljskiObjekat")]
        public async Task<IActionResult> CreateUgostiteljskiObjekat(UgostiteljskiObjekatDTO objekatDTO)
        {
            var id = await _authRepository.Register(objekatDTO.ugostiteljskiObjekat, objekatDTO.password);
            if(id == null)
                return BadRequest("Documnet already exists in the database");
    
            return StatusCode(201, "Document ugostiteljskiObjekat has been created in the database with id: " + id);
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginDTO loginInfo)
        {
            var token = await _authRepository.Login(loginInfo.username, loginInfo.password);
            if(token == null)
                return BadRequest("Wrong username or password");
            return Ok(token);
        }

        [HttpGet]
        [Route("UzmiSadrzajFiltera")]
        public async Task<FilterSadrzaj> GetFilterSadrzaj()
        {
            FilterSadrzaj result = new FilterSadrzaj();
            result.oznake = _objekti.Distinct(new StringFieldDefinition<UgostiteljskiObjekat, string>("oznake"), o => o.role == "Objekat").ToList();
            result.stolovi = await _objekti.Distinct(new StringFieldDefinition<UgostiteljskiObjekat, int>("stolovi.brojOsoba"), o => o.role == "Objekat").ToListAsync();
            result.mesta = await _objekti.AsQueryable<UgostiteljskiObjekat>().Where(o => o.role == "Objekat")
                                            .Select(objekat => objekat.adresa.grad).Distinct().ToListAsync();
            return result;
        }

        [HttpPost]
        [Route("UzmiUgostiteljskeObjekte")]
        public async Task<List<GetUgostiteljskiObjekatDTO>> GetUgostiteljskeObjekate([FromBody]Filter filter)
        {
            /* Filter filter = new Filter
            {
                filtriraj = true,
                oznake = new List<string> {"kafic", "restoran"},
                mesta = new List<string> {"Niš", "Beograd"},
                stolovi = new List<int> {2, 8},
                filtrirajSlobodnaMesta = false,
                imaSlobodnihMesta = true,
                sortiraj = Sort.NazivAZ
            }; */

            List<FilterDefinition<UgostiteljskiObjekat>> filteri = new List<FilterDefinition<UgostiteljskiObjekat>>();
            var builder = Builders<UgostiteljskiObjekat>.Filter;
            var sortBuilder = Builders<UgostiteljskiObjekat>.Sort;
            filteri.Add(builder.Eq(objekat => objekat.role, "Objekat"));
            SortDefinition<UgostiteljskiObjekat> sort = null;
            if(filter.filtriraj)
            {
                if(filter.oznake != null && filter.oznake.Count > 0)
                {
                    List<FilterDefinition<UgostiteljskiObjekat>> filteriOznake = new List<FilterDefinition<UgostiteljskiObjekat>>();
                    foreach(string oznaka in filter.oznake)
                    {
                        filteriOznake.Add(builder.AnyEq(objekat => objekat.oznake, oznaka));
                    }
                    filteri.Add(builder.Or(filteriOznake));
                }

                if(filter.mesta != null && filter.mesta.Count > 0)
                {
                    List<FilterDefinition<UgostiteljskiObjekat>> filteriMesta = new List<FilterDefinition<UgostiteljskiObjekat>>();
                    foreach(string mesto in filter.mesta)
                    {
                        filteriMesta.Add(builder.Eq(objekat => objekat.adresa.grad, mesto));
                    }
                    filteri.Add(builder.Or(filteriMesta));
                }

                if(filter.stolovi != null && filter.stolovi.Count > 0)
                {
                    List<FilterDefinition<UgostiteljskiObjekat>> filteriStolovi = new List<FilterDefinition<UgostiteljskiObjekat>>();
                    foreach(int brOsoba in filter.stolovi)
                    {
                        filteriStolovi.Add(builder.ElemMatch(objekat => objekat.stolovi, sto => sto.brojOsoba == brOsoba));
                    }
                    filteri.Add(builder.Or(filteriStolovi));
                }

                if(filter.filtrirajSlobodnaMesta)
                {
                    if(filter.imaSlobodnihMesta)
                        filteri.Add(builder.ElemMatch(objekat => objekat.stolovi, sto => sto.slobodnihStolova > 0));
                    else
                        filteri.Add(builder.Not(builder.ElemMatch(objekat => objekat.stolovi, sto => sto.slobodnihStolova > 0)));
                }

                if(filter.sortiraj != Sort.None)
                {
                    switch(filter.sortiraj)
                    {
                        case Sort.NazivAZ:
                            sort = sortBuilder.Ascending(objekat => objekat.ime);
                            break;
                        case Sort.NazivZA:
                            sort = sortBuilder.Descending(objekat => objekat.ime);
                            break;
                    }
                }

            }
            var query = builder.And(filteri);

            var objekti = await _objekti.Find(query).Sort(sort).ToListAsync();
            return objekti.Select(o => _mapper.Map<GetUgostiteljskiObjekatDTO>(o)).ToList();
        }

        [HttpGet]
        [Route("UzmiUgostiteljskiObjekat/{id}")]
        public async Task<GetUgostiteljskiObjekatDTO> GetUgostiteljskiObjekat(string id)
        {
            UgostiteljskiObjekat objekat = await _objekti.Find(objekat => objekat.Id == id).FirstOrDefaultAsync();
            return _mapper.Map<GetUgostiteljskiObjekatDTO>(objekat); 
        }

        [Authorize(Roles = "Objekat,Admin")]
        [HttpDelete]
        [Route("ObrisiUgostiteljskiObjekat/{id}")]
        public async Task<IActionResult> DeleteUgostiteljskiObjekat(string id)
        {
            string authenticatedRole = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
            if(authenticatedRole.Equals("Objekat"))
            {
                string authenticatedId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
                if(authenticatedId != id)
                    return StatusCode(403, "You are not allowed to delete other users!");
            } else
            {
                UgostiteljskiObjekat obj = await _objekti.Find(objekat => objekat.Id == id).FirstOrDefaultAsync();
                if(obj == null)
                {
                    return NotFound("Requsted document doesn't exist in the database!");
                }
            }
            
            await _dogadjaji.DeleteManyAsync(dogadjaj => dogadjaj.ugostiteljskiObjekat == id);
            await _objekti.DeleteOneAsync(objekat => objekat.Id == id);
            
            return Ok("Obrisan ugostiteljski objekat id: " + id);
        }

        [Authorize(Roles = "Objekat")]
        [HttpPut]
        [Route("IzmeniBrojSlobodnihStolova/{id}")]
        public async Task<IActionResult> UpdateSlobodniStolovi(string id, List<Stolovi> NoviStolovi)
        {
            string authenticatedId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            if(authenticatedId != id)
                return StatusCode(403, "You are not allowed to edit other users data!");

            var update = Builders<UgostiteljskiObjekat>.Update.Set(objekat => objekat.stolovi, NoviStolovi);
            await _objekti.UpdateOneAsync(objekat => objekat.Id == id, update);
            return Ok("Document ugostiteljskiObjekat id: " + id + " has been updated in the database");
        }

        [Authorize(Roles = "Objekat")]
        [HttpPut]
        [Route("IzmeniUgostiteljskiObjekat")]
        public async Task<IActionResult> UpdateUgostiteljskiObjekat(UgostiteljskiObjekat objekat)
        {
            string authenticatedId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            if(objekat.Id != authenticatedId)
                return Unauthorized("You are not allowed to edit other users data");
                
            UgostiteljskiObjekat obj = await _objekti
                    .Find(o => o.Id == objekat.Id 
                        && o.username.ToLower() == objekat.username.ToLower()).FirstOrDefaultAsync();
            if(obj == null)
                return Unauthorized("You are not allowed to edit username");

            objekat.passwordHash = obj.passwordHash;
            objekat.passwordSalt = obj.passwordSalt;
            objekat.role = obj.role;
            objekat.dogadjaji = obj.dogadjaji;

            await _objekti.ReplaceOneAsync(o => o.Id == objekat.Id, objekat);
            
            return Ok("Document ugostiteljskiObjekat id: " + objekat.Id + " has been updated in the database");
        }

        [Authorize(Roles = "Objekat")]
        [HttpPut]
        [Route("PromeniSifru")]
        public async Task<IActionResult> UpdatePassword(UgostiteljskiObjekatDTO objekatDTO)
        {
            string authenticatedId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            if(objekatDTO.ugostiteljskiObjekat.Id != authenticatedId)
                return Unauthorized("You are not allowed to edit other users password!");
                
            string id = await _authRepository.Update(objekatDTO.ugostiteljskiObjekat.Id, objekatDTO.password);
            
            return Ok("Password for document ugostiteljskiObjekat id: " + id + " has been updated in the database!");
        }
    }
}
